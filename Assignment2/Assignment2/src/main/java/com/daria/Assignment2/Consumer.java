package com.daria.Assignment2;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

public class Consumer {
    private final static String QUEUE_NAME = "patient";
    private final static long HOURS_12 = 43200000;
    private final static long HOUR_1 = 3600000;

    public void receive() {

        System.out.println("[*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[x] Received '" + message + "'");
            Activity recAct = getActivityFromString(message);
            checkRules(recAct);
        };
        try {
            Broker.channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkRules(Activity activity) {
        String report = "";
        if (activity.getActivity().equals("Sleeping")){
            if (activity.getEndDate() - activity.getStartDate() > HOURS_12) {
                report += "Something might be wrong with patient: " + activity.getPatient_id() + "\n";
                report += "[X] The patient is sleeping for more than 12 hours!";
            }
        }

        if (activity.getActivity().equals("Toileting") || activity.getActivity().equals("Grooming") || activity.getActivity().equals("Showering")) {
            if (activity.getEndDate() - activity.getStartDate() > HOUR_1) {
                report += "Something might be wrong with patient: " + activity.getPatient_id() + "\n";
                report += "[X] The patient has been in the bathroom for more than 1 hour!";
            }
        }

        if (activity.getActivity().equals("Leaving")){
            if (activity.getEndDate() - activity.getStartDate() > HOURS_12) {
                report += "Something might be wrong with patient: " + activity.getPatient_id() + "\n";
                report += "[X] The patient is away/outdoor for more than 12 hours!";
            }
        }
        System.err.println(report);
    }

    private Activity getActivityFromString (String message) {
        Activity activity = new Activity();
        message = message.replaceAll("}", "");
        message = message.replaceAll("\"", "");

        String[] pairs = message.split(",");

        String[] patientId = pairs[0].split(":");
        activity.setPatient_id(patientId[1]);

        String[] act = pairs[1].split(":");
        activity.setActivity(act[1]);

        String[] start = pairs[2].split(":");
        activity.setStartDate(Long.parseLong(start[1]));

        String[] end = pairs[3].split(":");
        activity.setEndDate(Long.parseLong(end[1]));

        return activity;
    }


}
