package com.daria.Assignment2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class Assignment2Application {

	public static void main(String[] args) {
		Broker.startBroker();
		Producer producer = new Producer();
		producer.sendDataFromFile();
		Consumer consumer = new Consumer();
		consumer.receive();
		SpringApplication.run(Assignment2Application.class, args);
	}

}
