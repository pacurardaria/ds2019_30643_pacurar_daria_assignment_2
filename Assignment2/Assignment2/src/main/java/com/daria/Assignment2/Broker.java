package com.daria.Assignment2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Broker {
    private static ConnectionFactory factory = new ConnectionFactory();
    private final static String QUEUE_NAME = "patient";
    public static Channel channel;

    public static void startBroker() {
        factory.setHost("localhost");
        Connection connection;
        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

    }
}
