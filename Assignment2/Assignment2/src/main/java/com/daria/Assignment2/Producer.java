package com.daria.Assignment2;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.utility.RandomString;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Producer {

    private final static String QUEUE_NAME = "patient";
    private ObjectMapper mapper = new ObjectMapper();

    public void sendDataFromFile() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("E:\\FACULTATE\\An 4\\SD\\ds2019_30643_pacurar_daria_assignment_2\\activity.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("COULDN'T OPEN FILE!");
            e.printStackTrace();
        }
        while (true) {
            String line;
            try {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                String[] parts = line.split("\t\t");

                Activity activity = new Activity();
                activity.setPatient_id(RandomString.make());
                activity.setStartDate(parseDateToLong(parts[0]));
                activity.setEndDate(parseDateToLong(parts[1]));
                activity.setActivity(parts[2]);

                Broker.channel.basicPublish("", QUEUE_NAME, null, mapper.writeValueAsBytes(activity));
                System.out.println(" [x] Sent '" + mapper.writeValueAsString(activity) + "'");

                Thread.sleep(100);

            } catch (IOException| InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            reader.close();
        } catch (IOException e) {
            System.out.println("COULDN'T CLOSE FILE!");
        }
    }

    private long parseDateToLong(String toParse)  {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(toParse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

}
